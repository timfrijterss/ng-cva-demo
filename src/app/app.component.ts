import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'ng-cva-demo';
  customInputFormGroup?: FormGroup;

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.customInputFormGroup = this.formBuilder.group({
      // customInput: new FormControl({ value: null, disabled: true }),
      customInput: null,
      matCustomInput: null,
      filledCompositeAddress: new FormControl({
        street: 'test',
        houseNumber: 1,
        bus: 'F',
        zipCode: '1234',
        city: 'test',
        country: 'Nederland',
      }),
      emptyCompositeAddress: null,
      busRequiredCompositeAddress: null,
      layoutCompositeAddress: null,
    });
  }

  writeValue(value: string): void {
    this.customInputFormGroup.controls.customInput.setValue(value);
  }
}
