import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CustomInputComponent } from './custom-input/custom-input.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCustomInputComponent } from './mat-custom-input/mat-custom-input.component';
import { MatIconModule } from '@angular/material/icon';
import { MatCompositeCvaComponent } from './mat-composite-cva/mat-composite-cva.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCompositeDifferentLayoutCvaComponent } from './mat-composite-different-layout-cva/mat-composite-different-layout-cva.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomInputComponent,
    MatCustomInputComponent,
    MatCompositeCvaComponent,
    MatCompositeDifferentLayoutCvaComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
