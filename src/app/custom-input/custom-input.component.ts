import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NgControl,
  ValidationErrors,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-custom-input',
  templateUrl: './custom-input.component.html',
  styleUrls: ['./custom-input.component.scss'],
})
export class CustomInputComponent implements OnInit, ControlValueAccessor {
  disabled: boolean;
  @ViewChild('input', { static: true }) input: ElementRef;
  onTouched!: () => void;
  onChange!: (_value: string | null) => void;

  /**
   *  Injecting NgControl so formControlName, formControl directive and NgModel
   *  all work easily. They all provide themselves as NgControl
   */
  constructor(
    @Self() public controlDir: NgControl,
    private readonly renderer: Renderer2
  ) {
    controlDir.valueAccessor = this;
  }

  /**
   *  Validate if input has a length of 4 or more
   */
  static myValidationStuff(ctrl: AbstractControl): ValidationErrors {
    if (ctrl.value != null && ctrl.value.length >= 4) {
      return null;
    } else {
      return { notLongEnough: true };
    }
  }

  /**
   * Writes a new value to the DOM element(s).
   *
   * This method is called by the forms API to write to the view when programmatic
   * changes from MODEL to VIEW are requested.
   */
  writeValue(value: string): void {
    // Used Renderer2 because of https://angular.io/api/core/ElementRef#properties
    console.log(`writeValue is called with ${value}`);
    this.renderer.setProperty(this.input.nativeElement, 'value', value);
  }

  /**
   * This method is called by the forms API on initialization to update the form
   * model when values propagate from the VIEW to the MODEL.
   */
  registerOnChange(fn: any): void {
    this.onChange = fn;
    console.log('registerOnChange called');
  }

  /**
   * The form API is passing us the function it wants us to call
   * when the input is touched.
   */
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
    console.log('registerOnTouched called');
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
    console.log('setDisabledState called');
  }

  validate(ctrl: AbstractControl): Array<ValidationErrors> | null {
    const myValidation = CustomInputComponent.myValidationStuff(ctrl);
    const validRequired = Validators.required(ctrl);

    return !myValidation && !validRequired
      ? null
      : [myValidation, validRequired];
  }

  ngOnInit(): void {
    console.log('ngAfterContentInit called');
    const control = this.controlDir.control;
    const validators = control.validator
      ? [control.validator, this.validate]
      : this.validate;
    control.setValidators(validators);
    control.updateValueAndValidity();
  }
}
