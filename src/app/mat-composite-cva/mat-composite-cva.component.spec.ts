import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatCompositeCvaComponent } from './mat-composite-cva.component';

describe('MatCompositeCvaComponent', () => {
  let component: MatCompositeCvaComponent;
  let fixture: ComponentFixture<MatCompositeCvaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatCompositeCvaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatCompositeCvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
