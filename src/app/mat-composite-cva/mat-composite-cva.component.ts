import { Component, DoCheck, Input, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  ControlValueAccessor,
  FormBuilder, FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, ValidationErrors,
  Validator, Validators
} from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-mat-composite-cva',
  templateUrl: './mat-composite-cva.component.html',
  styleUrls: ['./mat-composite-cva.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: MatCompositeCvaComponent,
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: MatCompositeCvaComponent,
      multi: true,
    },
  ],
})
export class MatCompositeCvaComponent
  implements ControlValueAccessor, Validator, OnDestroy, OnInit {
  @Input() disabled?: boolean;
  @Input() formControlName!: string;
  @Input() isBusRequired = false;
  addressFormGroup: FormGroup;
  readonly MIN_HOUSE_NUMBER = 1;
  subscriptions: Array<Subscription> = [];

  onTouched!: () => void;

  constructor(
    private readonly controlContainer: ControlContainer,
    private readonly formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.addressFormGroup = this.createEmptyForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  registerOnChange(fn: any): void {
    this.subscriptions.push(this.addressFormGroup.valueChanges.subscribe(fn));
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(val: any): void {
    if (val) {
      this.addressFormGroup.setValue(val, { emitEvent: false });
    }
  }

  setDisabledState(isDisabled: boolean): void {
    isDisabled
      ? this.addressFormGroup.disable({ emitEvent: false })
      : this.addressFormGroup.enable({ emitEvent: false });
  }

  validate(_c: AbstractControl): ValidationErrors | null {
    return this.addressFormGroup.valid ? null : { invalid: true };
  }

  registerOnValidatorChange(fn: () => void): void {
    this.subscriptions.push(this.addressFormGroup.statusChanges.subscribe(fn));
  }

  private createEmptyForm(): FormGroup {
    return this.formBuilder.group({
      street: new FormControl('', Validators.required),
      houseNumber: new FormControl('', [
        Validators.required,
        Validators.min(this.MIN_HOUSE_NUMBER),
      ]),
      bus: new FormControl('', this.isBusRequired ? Validators.required : []),
      zipCode: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      country: new FormControl('België', Validators.required),
    });
  }
}
