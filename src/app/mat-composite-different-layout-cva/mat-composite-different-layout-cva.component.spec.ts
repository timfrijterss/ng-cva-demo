import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatCompositeDifferentLayoutCvaComponent } from './mat-composite-different-layout-cva.component';

describe('MatCompositeDifferentLayoutCvaComponent', () => {
  let component: MatCompositeDifferentLayoutCvaComponent;
  let fixture: ComponentFixture<MatCompositeDifferentLayoutCvaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatCompositeDifferentLayoutCvaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatCompositeDifferentLayoutCvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
