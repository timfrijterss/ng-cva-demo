import { Component } from '@angular/core';
import { MatCompositeCvaComponent } from '../mat-composite-cva/mat-composite-cva.component';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-mat-composite-different-layout-cva',
  templateUrl: './mat-composite-different-layout-cva.component.html',
  styleUrls: ['./mat-composite-different-layout-cva.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: MatCompositeDifferentLayoutCvaComponent,
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: MatCompositeDifferentLayoutCvaComponent,
      multi: true,
    },
  ],
})
export class MatCompositeDifferentLayoutCvaComponent extends MatCompositeCvaComponent {

}
