import { Component, ElementRef, OnInit, Self, ViewChild } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NgControl,
  ValidationErrors,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-mat-custom-input',
  templateUrl: './mat-custom-input.component.html',
  styleUrls: ['./mat-custom-input.component.scss'],
})
export class MatCustomInputComponent implements ControlValueAccessor, OnInit {
  @ViewChild('input', { static: true }) input: ElementRef;
  onTouched!: () => void;
  onChange!: (_value: string | null) => void;
  formControl: FormControl;

  /**
   *  Injecting NgControl so formControlName, formControl directive and NgModel
   *  all work easily. They all provide themselves as NgControl
   */
  constructor(@Self() public controlDir: NgControl) {
    controlDir.valueAccessor = this;
    this.formControl = new FormControl(null);
  }

  /**
   *  Validate if input has a length of 4 or more
   */
  static myValidationStuff(ctrl: AbstractControl): ValidationErrors {
    if (ctrl.value != null && ctrl.value.length >= 4) {
      return null;
    } else {
      return { notLongEnough: true };
    }
  }

  writeValue(value: any): void {
    console.log('writeValue is called');
    this.formControl.setValue(value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    console.log('registerOnChange called');
  }

  // The form is passing us the function it wants us to call
  // when the input is touched. We need to save this somewhere.
  // This is called on onBlur event.
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
    console.log('registerOnTouched called');
  }

  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.formControl.disable() : this.formControl.enable();
    console.log('setDisabledState called');
  }

  // let's set up some custom validation.  It's easier, in my opinion
  // to handle all validation calls that we want mandatory right
  // here in our validate call.  Then it's just some basic logic.
  validate(ctrl: AbstractControl): Array<ValidationErrors> | null {
    const myValidation = MatCustomInputComponent.myValidationStuff(ctrl);
    const validRequired = Validators.required(ctrl);

    return !myValidation && !validRequired
      ? null
      : [myValidation, validRequired];
  }

  ngOnInit(): void {
    console.log('ngAfterContentInit called');
    const control = this.controlDir.control;
    const validators = control.validator
      ? [control.validator, this.validate]
      : this.validate;
    this.formControl.setValidators(validators);
    this.formControl.updateValueAndValidity();
  }
}
